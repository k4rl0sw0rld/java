package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
		int i = 0,n;
		int max = 0, currentNum;

		Scanner scanner = new Scanner(System.in);

		System.out.print("input n: ");
		n = scanner.nextInt();

		while(i < n){
			System.out.print("input current number: ");
			currentNum = scanner.nextInt();
			if(i==0){
				max = currentNum;
			}
			else{
				if(currentNum>max){
					max = currentNum;
				}
			}
			i++;
		}
		System.out.print("max number = "+max);
    }
}
